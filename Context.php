<?php
/**
 * Created by PhpStorm.
 * User: jacknguyen
 * Date: 10/25/18
 * Time: 3:04 PM
 */

class Context
{
    private $method_has_body = array('PUT', 'POST');
    public $request_method;
    public $request_uri;
    public $path_params = array();
    public $query_params = array();
    public $cookie;


    public function __construct()
    {
        $this->request_method = $_SERVER['REQUEST_METHOD'];
        $this->request_uri = $_SERVER['REQUEST_URI'];
        if(array_key_exists('QUERY_STRING', $_SERVER)){
            parse_str($_SERVER['QUERY_STRING'], $this->query_params);
        }
        $this->cookie = $_COOKIE;
    }

    public function getBody(){
        if (array_search($this->request_method, $this->method_has_body)) {
            return json_decode(file_get_contents('php://input'));
        }
        return null;
    }

    public function send($text){
        http_response_code(200);
        echo $text;
    }

    public function sendJSON($content){
        header('Content-Type: application/json');
        echo json_encode($content);
    }
}