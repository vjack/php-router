<?php
/**
 * Created by PhpStorm.
 * User: jacknguyen
 * Date: 10/25/18
 * Time: 3:22 PM
 */

include_once 'Router.php';

$router = new Router();


$router->include(function ($ctx, $next) {
    echo 'Check Authorization' . PHP_EOL;
    $next();
}, function ($ctx, $next) {
    $ctx->send('Multiple funcs' . PHP_EOL);
    $next();
});

$router->include(function ($ctx, $next) {
    echo 'CORS' . PHP_EOL;
    $next();
});


$router->get('/resources/{resId}/users/{userId}',
    function ($ctx, $next) {
        $ctx->send('Parameters Validate' . PHP_EOL);
        $next();
    },
    function ($ctx) {
        $ctx->sendJSON(array('Query Params' => $ctx->query_params, 'Path Params' => $ctx->path_params));
    }
);

$router->post('/resources', function ($ctx){
    $ctx->sendJSON($ctx->getBody());
});