<?php
/**
 * Created by PhpStorm.
 * User: jacknguyen
 * Date: 10/25/18
 * Time: 2:21 PM
 */


include_once 'Middleware.php';
include_once 'Context.php';
include_once 'Route.php';

class Router
{

    private $middleware;
    private $supported_methods = array('GET', 'PUT', 'POST', 'DELETE');
    private $context;
    private $routes;

    public function __construct()
    {
        $this->middleware = new Middleware();
        $this->context = new Context();
        $this->routes = array();
    }


    public function include()
    {
        $this->middleware->push(func_get_args());
    }

    public function __call($name, $arguments)
    {
        if (array_search(strtoupper($name), $this->supported_methods) !== false && count($arguments) > 1 && is_string($arguments[0])) {

            $route = new Route($arguments[0], array_slice($arguments, 1));

            if (!in_array($name, $this->routes)) {
                $this->routes[strtolower($name)] = array($route);
            } else {
                $this->routes[strtolower($name)][] = $route;
            }
        }
    }

    function nextMiddleware()
    {
        $method = strtolower($this->context->request_method);
        $func = $this->middleware->getNext();
        if ($func != null) {
            $func($this->context, array($this, 'nextMiddleware'));
        } else {
            if (array_key_exists($method, $this->routes)) {
                foreach ($this->routes[$method] as $route) {
                    if ($route->match($this->context->request_uri)) {
                        $route->execute($this->context);
                        break;
                    }
                }
            }
        }
    }

    private function execute()
    {

        $this->nextMiddleware();

    }

    public function __destruct()
    {
        $this->execute();
    }

}