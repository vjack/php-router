<?php
/**
 * Created by PhpStorm.
 * User: jacknguyen
 * Date: 10/26/18
 * Time: 9:56 AM
 */

class Route
{
    public $regex_path;
    public $middleware;
    private $path_params_name;
    private $path_params_value;
    private $pattern = '/\{[^\/?]+\}/';
    private $context;

    public function __construct($path, $middleware)
    {
        $this->middleware = new Middleware();
        $this->middleware->push($middleware);
        preg_match_all($this->pattern, $path, $params);
        foreach ($params[0] as $param) {
            $this->path_params_name[] = preg_replace('/[{}]/', '', $param);
        }
        $this->regex_path = '/^' . str_replace('/', '\/', preg_replace($this->pattern, '([^/?]+)', $path)) . '.*$/';
    }

    public function match($url)
    {
        if (preg_match($this->regex_path, $url)) {
            preg_match_all($this->regex_path, $url, $params);
            for ($index = 1; $index < count($params); $index++) {
                foreach ($params[$index] as $param) {
                    $this->path_params_value[] = $param;
                }
            }
            return true;
        }
        return false;
    }

    public function nextMiddleware()
    {
        $func = $this->middleware->getNext();
        if ($func != null) {
            $func($this->context, array($this, 'nextMiddleware'));
        }
    }

    public function execute($ctx)
    {
        $this->context = $ctx;
        for($i = 0 ; $i < count($this->path_params_name); $i++){
            $this->context->path_params[$this->path_params_name[$i]] = $this->path_params_value[$i];
        }
        $this->nextMiddleware();
    }
}