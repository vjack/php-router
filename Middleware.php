<?php
/**
 * Created by PhpStorm.
 * User: jacknguyen
 * Date: 10/25/18
 * Time: 3:08 PM
 */

class Middleware
{
    private $currentIndex = -1;
    private $content = array();

    function push(){
        $args = func_get_args();
        foreach ($args as $func){
            if(is_callable($func)){
                $this->content[] = $func;
            } else if(is_array($func)){
                $this->content = array_merge($this->content, array_filter($func, 'is_callable'));
            }
        }
    }

    function hasMore(){
        return $this->currentIndex < count($this->content) - 1;
    }

    function getNext(){
        if($this->hasMore()){
            $this->currentIndex++;
            return $this->content[$this->currentIndex];
        }

        return null;
    }
}